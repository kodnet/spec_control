package jp.co.kodnet.confluence.plugins.event;

import com.atlassian.confluence.pages.Page;
import com.atlassian.event.Event;

public class SpecialControUpdateEvent extends Event {

    private Page page;

    public SpecialControUpdateEvent(Page page) {
        super(page);
    }

    public Page getPage() {
        return page;
    }
}

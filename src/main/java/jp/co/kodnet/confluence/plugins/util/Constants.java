package jp.co.kodnet.confluence.plugins.util;

public class Constants {
    public static final String SPECIAL_CONTROL_PAGE_TITLE = "Spec Control";
    public static final String SPECIAL_CONTROL_PAGE_LABEL = "specialcontrol";
    public static final String LABEL_TBD = "tbd";
}

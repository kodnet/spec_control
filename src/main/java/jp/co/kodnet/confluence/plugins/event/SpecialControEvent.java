package jp.co.kodnet.confluence.plugins.event;

import com.atlassian.confluence.event.events.content.page.PageCreateEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.pages.Page;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import jp.co.kodnet.confluence.plugins.util.CommonFunction;
import org.springframework.beans.factory.DisposableBean;

public class SpecialControEvent implements DisposableBean {

    protected EventPublisher eventPublisher;

    public SpecialControEvent(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        eventPublisher.register(this);
    }

    @EventListener
    public void createPageEvent(PageCreateEvent event) throws Exception {
        Page page = event.getPage();

        boolean isPageUi = CommonFunction.isPageSpecialControl(page);
        if (isPageUi == true) {
            CommonFunction.addRichTextForMacroFromPageSpecialControl(page);
        } else {
            CommonFunction.addRichTextForMacro(page.getId());
        }
    }

    @EventListener
    public void pageUpdateEvent(PageUpdateEvent event) throws Exception {
        Page page = event.getPage();

        boolean isPageUi = CommonFunction.isPageSpecialControl(page);
        if (isPageUi == true) {
            CommonFunction.addRichTextForMacroFromPageSpecialControl(page);
        } else {
            CommonFunction.addRichTextForMacro(page.getId());
        }
    }

    @EventListener
    public void specialTagMacroUpdateEvent(SpecialControUpdateEvent event) throws Exception {
        Page page = event.getPage();
        if (page == null) {
            return;
        }

       boolean isPageUi = CommonFunction.isPageSpecialControl(page);
        if (isPageUi == true) {
            CommonFunction.addRichTextForMacroFromPageSpecialControl(page);
        } else {
            CommonFunction.addRichTextForMacro(page.getId());
        }
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}

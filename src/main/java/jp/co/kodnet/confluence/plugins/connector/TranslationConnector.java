package jp.co.kodnet.confluence.plugins.connector;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import org.apache.commons.beanutils.MethodUtils;

/**
 *
 * @author TamPT
 */
public class TranslationConnector {

    public static final String TRANSLATION_PLUGIN_KEY = "jp.co.kodnet.confluence.plugins.KollaboTranslation";
    public static final String TRANSLATION_PUBLIC_SERVICE = "translationPublicService";

    public static final String METHOD_GET_BASE_SPACE_KEY_BY_SPACE_KEY = "getBaseSpaceKeyBySpaceKey";
    public static final String METHOD_GET_ALL_LANGUAGE_BY_SPACE_KEY = "getAllInfoLanguageBySpaceKey";
    

    public static Plugin getEnabledPlugin() throws Exception {
        try {

            PluginAccessor pluginAccessor = SpecialControlFactory.getPluginAccessor();
            Plugin plugin = pluginAccessor.getEnabledPlugin(TRANSLATION_PLUGIN_KEY);
            if (plugin == null) {
                return null;
            }

            return plugin;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean hasEnabledPlugin() throws Exception {
        Plugin plugin = getEnabledPlugin();
        return plugin != null;
    }

    public static Object execute(String method, Object... param) throws Exception {
        try {
            Plugin plugin = getEnabledPlugin();
            if (plugin == null) {
                return null;
            }

            ModuleDescriptor moduleDescriptor = plugin.getModuleDescriptor(TRANSLATION_PUBLIC_SERVICE);
            if (moduleDescriptor == null) {
                return null;
            }

            Object value = MethodUtils.invokeMethod(moduleDescriptor.getModule(), method, param);
            if (value == null) {
                return null;
            }

            return value;
        } catch (Exception e) {
            return null;
        }
    }
}

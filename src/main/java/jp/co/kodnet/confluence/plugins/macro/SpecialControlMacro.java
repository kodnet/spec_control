package jp.co.kodnet.confluence.plugins.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.editor.macro.PlaceholderUrlFactory;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.thumbnail.Dimensions;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.spring.container.ContainerManager;
import java.net.URLEncoder;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import jp.co.kodnet.confluence.plugins.util.CommonFunction;
import jp.co.kodnet.confluence.plugins.util.SpecialControlLog;
import org.apache.commons.lang.StringUtils;

public class SpecialControlMacro implements Macro, EditorImagePlaceholder {

    public static final String TITLE_PAGE_UI_CONTROL = "uicontrol";
    public static final String UI_CONTROL_TITLE_PAGE_LABEL = "uicontrol";
    public static final String LABEL_TBD = "tbd";
    private PlaceholderUrlFactory placeholderUrlFactory = (PlaceholderUrlFactory) ContainerManager.getComponent("placeholderUrlFactory");

    public ImagePlaceholder getImagePlaceholder(Map<String, String> params, ConversionContext ctx) {
        ImagePlaceholder imagePlaceholder = null;
        try {
            String currentSpaceKey = "";
            String pageLabel ="";
            if (ctx.getEntity() instanceof Draft) {
                Draft draft = ((Draft) ctx.getEntity());
                if (draft == null) {
                    return imagePlaceholder;
                }
                currentSpaceKey = draft.getDraftSpaceKey();
            } else {
                Page page = ((Page) ctx.getEntity());
                if (page == null) {
                    return imagePlaceholder;
                }
                for (Label lb : page.getLabels()) {
                    pageLabel += lb.getName() + "$$$";
                }
 
                currentSpaceKey = page.getSpaceKey();
            }
            
            String specialTagKey = params.get("specialTagKey");
            SettingsManager settingsManager = SpecialControlFactory.getSettingsManager();
            String url = settingsManager.getGlobalSettings().getBaseUrl() + "/rest/specialcontrol/1.0/get?spaceKey="
                    + URLEncoder.encode(currentSpaceKey, "UTF-8")
                    + "&specialTagKey="
                    + URLEncoder.encode(specialTagKey, "UTF-8")
                    + "&pageLabel="
                    + URLEncoder.encode(pageLabel, "UTF-8")
                    + "&random="
                    + Math.random();
            
            imagePlaceholder = new DefaultImagePlaceholder(url, new Dimensions(100, 31), false);
            return imagePlaceholder;
        } catch (Exception ex) {
            SpecialControlLog.outputLog(ex.getMessage());
            return imagePlaceholder;
        }
    }
    
    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        // パラメーターの取得
        String currentSpaceKey = "";
        
        if (conversionContext.getEntity() instanceof Draft) {
            currentSpaceKey = ((Draft) conversionContext.getEntity()).getDraftSpaceKey();
        } else {
            currentSpaceKey = ((Page) conversionContext.getEntity()).getSpaceKey();
        }

        String specialTagKey = parameters.get("specialTagKey");
        if (specialTagKey == null || specialTagKey.isEmpty()) {
            return StringUtils.EMPTY;
        }
        Page page = ((Page) conversionContext.getEntity());
        String pageLabel = "";
        for (Label lb : page.getLabels()) {
            pageLabel += lb.getName() + "$$$";
        }
        
        try {
            boolean isPageView = true;
            String specialValue = CommonFunction.getDisplayValueOfMacro(specialTagKey, currentSpaceKey, pageLabel, isPageView);
            body = "<span class = 'ui-higlight-special-value' style = 'background: #ffe5b9;'>" + specialValue + "</span>";
        } catch (Exception e) {
            Logger.getLogger(SpecialControlMacro.class.getName()).log(Level.SEVERE, null, e);
        }
        
        return body;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}

package jp.co.kodnet.confluence.plugins.util;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.spring.container.ContainerManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.co.kodnet.confluence.plugins.connector.TranslationConnector;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CommonFunction {

    public static Date getCurentDate() {
        return new Date();
    }

    public static String getDisplayValueOfMacro(String macroInputValue, String curentSpaceKey, String pageLabel, Boolean isPageView) throws Exception {
        // Key is Not exists
        String specialTag = null;
        Object reflection = TranslationConnector.execute(TranslationConnector.METHOD_GET_BASE_SPACE_KEY_BY_SPACE_KEY, curentSpaceKey);
        
        if (reflection == null) {
            return macroInputValue;
        }
        String baseSpaceKey = (String) reflection;

        Page page = getSpecialControlPage(baseSpaceKey);
        if (page == null) {
            return macroInputValue;
        }
        
        specialTag = getSpecialControlByPageLabel(pageLabel, page.getBodyAsString(), null, macroInputValue, isPageView);
        if (("").equals(specialTag)) {
            return macroInputValue;
        }
        return specialTag;
    }

    public static Page getSpecialControlPage(String spaceKeyParam) {        
        PageManager pageManager = (PageManager) ContainerManager.getComponent("pageManager");
        Page page = pageManager.getPage(spaceKeyParam, Constants.SPECIAL_CONTROL_PAGE_TITLE);
        String body = "";
        if (page == null) {
            return page;
        }
        List<Label> labels = page.getLabels();
        if (labels.isEmpty()) {
            return null;
        }
        int count = 0;
        for (Label label : labels) {
            if (label.getDisplayTitle().equals(Constants.SPECIAL_CONTROL_PAGE_LABEL)) {
                count++;
            }
            if (label.getDisplayTitle().equals(Constants.LABEL_TBD)) {
                count++;
            }
        }
        if (count != 2) {
            return null;
        }
        return page;
    }

    public static String getSpecialControlByPageLabel(String pageLabel, String bodyFormat, String baseSpaceKey, String macroInputValue, Boolean isPageView) {
        String result = "";
        try {
            Document document = XmlDocumentUtil.convertStringToXmlWithoutUnEscapeHtmlEntities(bodyFormat, Boolean.TRUE);
            Element root = document.getDocumentElement();
            Element element = XmlDocumentUtil.getFirstElementByTagName(root, "tr");
            if (element == null) {
                return result;
            }            

            List<Element> thElements = XmlDocumentUtil.getElementByTagName(element, "th");
            int position = 1;
            if ("".equals(pageLabel)) {
                position = 1;
            } else {
                for (int i = 0; i < thElements.size(); i++) {
                    Element thElement = thElements.get(i);
                    String machineName = thElement.getTextContent();
                    if (machineName == null || ("").equals(machineName)) {
                        continue;
                    }                    
                    if (pageLabel.toLowerCase().contains(machineName.toLowerCase())) {
                        position = i;
                        break;
                    }
                }
            }

            //find lang column
            org.jsoup.nodes.Document docJsoup = Jsoup.parse(bodyFormat);
            String expression = "td:contains(" + macroInputValue + ")";
            Elements tdElement = docJsoup.select(expression);
            for (org.jsoup.nodes.Element item : tdElement) {
                String text = item.text();
                if (!text.equals(macroInputValue)) {
                    continue;
                }
                org.jsoup.nodes.Element trElement = item.parent();
                if (trElement != null) {
                    Elements el = trElement.select("td");
                    org.jsoup.nodes.Element value = el.get(position);
                    if (value != null) {
                        if (isPageView == true) {
                            List<org.jsoup.nodes.Node> nodes = value.childNodes();
                            StringBuilder str = new StringBuilder();
                            for (org.jsoup.nodes.Node node : nodes) {
                                str.append(node.outerHtml());
                            }
                            result = str.toString();
                        } else {
                            result = value.text();
                        }
                    }
                }
            }
        } catch (Exception e) {
            SpecialControlLog.outputLogException(CommonFunction.class, e);
        }
        return result;
    }

    public static boolean isPageSpecialControl(Page page) {        
        if(!page.getTitle().equals(Constants.SPECIAL_CONTROL_PAGE_TITLE)){
            return false;
        }
        
        List<Label> labels = page.getLabels();
        if (labels.isEmpty()) {
            return false;
        }
        int count = 0;
        for (Label label : labels) {
            if (label.getDisplayTitle().equals(Constants.SPECIAL_CONTROL_PAGE_LABEL)) {
                count++;
            }
            if (label.getDisplayTitle().equals(Constants.LABEL_TBD)) {
                count++;
            }
        }
        if (count != 2) {
            return false;
        }
        return true;
    }
    
    public static void addRichTextForMacro(Long pageId) {
        TransactionTemplate transactionTemplate = SpecialControlFactory.getTransactionTemplate();
        ContentEntityObjectDao contentEntityObjectDao = (ContentEntityObjectDao) ContainerManager.getInstance().getContainerContext().getComponent("contentEntityObjectDao");
        transactionTemplate.execute(new TransactionCallback<Boolean>() {
            @Override
            public Boolean doInTransaction() {
                try {
                    ContentEntityObject entity = contentEntityObjectDao.getById(pageId);
                    if ((entity instanceof Page) == false) {
                        return false;
                    }

                    String xmlPage = entity.getBodyAsString();
                    if (xmlPage.toLowerCase().contains("\"specialcontrolmacro\"") == false) {
                        return false;
                    }

                    Document document = XmlDocumentUtil.convertStringToXml(xmlPage, true);
                    List<Element> elements = XmlDocumentUtil.getElementsByTagNameAndAttr(document.getDocumentElement(), "ac:structured-macro", "ac:name", "specialcontrolmacro");
                    for (Element element : elements) {
                        Element specialKey = XmlDocumentUtil.getFirstElementByTagNameAndAttr(element, "ac:parameter", "ac:name", "specialTagKey");
                        if (specialKey == null) {
                            continue;
                        }

                        Element inline = XmlDocumentUtil.getFirstElementByTagNameAndAttr(element, "ac:parameter", "ac:name", "atlassian-macro-output-type");
                        if (inline == null) {
                            inline = document.createElement("ac:parameter");
                            inline.setAttribute("ac:name", "atlassian-macro-output-type");
                            element.appendChild(inline);
                        }

                        Element richText = XmlDocumentUtil.getFirstElementByTagName(element, "ac:rich-text-body");
                        if (richText != null) {
                            richText.getParentNode().removeChild(richText);
                        }

                        richText = document.createElement("ac:rich-text-body");
                        element.appendChild(richText);

                        Element p = document.createElement("p");
                        
                        Page page = (Page) entity;
                        String pageLabel = "";
                        for (Label lb : page.getLabels()) {
                            pageLabel += lb.getName() + "$$$";
                        }
                        String text = CommonFunction.getDisplayValueOfMacro(specialKey.getTextContent(), page.getSpaceKey(), pageLabel, true);
                        p.setTextContent(text);
                        richText.appendChild(p);
                    }

                    xmlPage = XmlDocumentUtil.convertXmlToString(document, true, false);
                    entity.setBodyAsString(xmlPage);
                    contentEntityObjectDao.saveRaw(entity);
                    return true;
                } catch (Exception e) {
                    Logger.getLogger(CommonFunction.class.getName()).log(Level.SEVERE, "UPDATE EXCEPTION", e);
                    return false;
                }
            }
        });
    }
    
    public static void addRichTextForMacroFromPageSpecialControl(Page page) {
        PageManager pageManager = SpecialControlFactory.getPageManager();
        SpaceManager spaceManager = SpecialControlFactory.getSpaceManager();
        try {
            List<Space> lstSpace = new ArrayList<>();
            Object reflection = TranslationConnector.execute(TranslationConnector.METHOD_GET_ALL_LANGUAGE_BY_SPACE_KEY, page.getSpaceKey());
            
            if (reflection == null) {
                lstSpace.add(page.getSpace());
            } else {
                Map<String, Object> reflectMap = (Map<String, Object>) reflection;                
                Map<String, Object> languages = (Map<String, Object>) reflectMap.get("languages");
                for (Map.Entry<String, Object> entry : languages.entrySet()) {
                    Map<String, Object> language = (Map<String, Object>)entry.getValue();
                    String spaceKey  = language.get("spaceKey").toString();
                    Space space = spaceManager.getSpace(spaceKey);
                    lstSpace.add(space);
                }
            }
            for (Space space : lstSpace) {
                if (space == null) {
                    continue;
                }
                List<Page> pages = pageManager.getPages(space, true);
                for (Page p : pages) {
                    String xmlPage = p.getBodyAsString();
                    if (xmlPage.toLowerCase().contains("\"specialcontrolmacro\"") == false) {
                        continue;
                    }
                    addRichTextForMacro(p.getId());
                }
            }

        } catch (Exception e) {
        }
    }
}

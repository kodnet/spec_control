package jp.co.kodnet.confluence.plugins.restapi;

import com.atlassian.confluence.content.render.xhtml.editor.macro.PlaceholderUrlFactory;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionBuilder;
import com.atlassian.spring.container.ContainerManager;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import jp.co.kodnet.confluence.plugins.util.CommonFunction;
import jp.co.kodnet.confluence.plugins.util.SpecialControlLog;

@Path("")
public class SpecialControlRest {

    @GET
    @Path("/get")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getImageBase64(@QueryParam("spaceKey") String spaceKey, @QueryParam("specialTagKey") String specialTagKey, @QueryParam("pageLabel") String pageLabel) {
        try {
            PlaceholderUrlFactory placeholderUrlFactory = (PlaceholderUrlFactory) ContainerManager.getComponent("placeholderUrlFactory");
            String specialValue = "";
            if (specialTagKey == null || specialTagKey.equals("")) {
                return Response.ok("").build();
            }
            boolean isPageView = false;
            specialValue = CommonFunction.getDisplayValueOfMacro(specialTagKey, spaceKey, pageLabel, isPageView);
            if(specialValue == null){
                return Response.ok("").build();
            }
            MacroDefinitionBuilder macroDefinitionBuilder = MacroDefinition.builder("SPEC");
            macroDefinitionBuilder.setDefaultParameterValue(specialValue);
            MacroDefinition macroDefinition = macroDefinitionBuilder.build();

            String src = placeholderUrlFactory.getUrlForMacro(macroDefinition);
            if (src == null || src.equals("")) {
                return Response.ok("").build();
            }
            SettingsManager settingsManager = SpecialControlFactory.getSettingsManager();

            String url = settingsManager.getGlobalSettings().getBaseUrl() + src;
            URL urlImage = new URL(url);

            BufferedImage image = ImageIO.read(urlImage);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", baos);
            byte[] bytes = baos.toByteArray();

            return Response.ok(bytes).header("Content-Disposition", "template; filename=" + specialValue + ".png").build();
        } catch (Exception ex) {
            SpecialControlLog.outputLog(ex.getMessage());
            return Response.ok("").build();
        }
    }
}

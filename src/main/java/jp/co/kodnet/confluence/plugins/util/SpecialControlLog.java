package jp.co.kodnet.confluence.plugins.util;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.util.ConfluenceHomeGlobalConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpecialControlLog {

    public static final String LOG_FOLDER_NAME = "SpecialControl";
    public static final String LOG_FILE_NAME = "SpecialControlLog";
    public static final String LOG_FILE_EXTENSION = "txt";
    private static String resultPath = "";

    public static void outputMemoryLog(String apiURL, String method, String resultCode, String errorCode, String errorDescription) {
        String mes = "";

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String strDate = null;
        try {
            strDate = sdf.format(CommonFunction.getCurentDate());
        } catch (Exception ex) {
            outputLogException(SpecialControlLog.class, ex);
        }
        resultCode = !StringUtils.isEmpty(resultCode) ? resultCode : "-";
        errorCode = !StringUtils.isEmpty(errorCode) ? errorCode : "-";
        errorDescription = !StringUtils.isEmpty(errorDescription) ? errorDescription : "-";
        mes += strDate + " " + method + " " + apiURL + " " + resultCode + " " + errorCode + " " + errorDescription;
        try {
            outputLog(mes);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SpecialControlLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void outputLogException(Class className, Exception ex) {
        try {
            outputLog(getStackTrace(ex));
        } catch (Exception ex1) {
            java.util.logging.Logger.getLogger(SpecialControlLog.class.getName()).log(Level.SEVERE, null, ex1);
        }
        Logger logger = LoggerFactory.getLogger(className);
        logger.error(ex.getMessage(), ex);
    }

    public static String getStackTrace(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static Map<String, Object> getBeanFaild(Exception ex) {
        Map<String, Object> mapBean = new HashMap<String, Object>();
        mapBean.put("result", false);
        mapBean.put("message", ex.getMessage());
        return mapBean;
    }

    public static void outputLog(String mes) {
        FileWriter fstream;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            String strDate = sdf.format(CommonFunction.getCurentDate());
            SimpleDateFormat sdfFile = new SimpleDateFormat("YYYY-MM");
            String strDateFileLog = sdfFile.format(CommonFunction.getCurentDate());
            resultPath = getDefaultLogPath() + "\\" + LOG_FOLDER_NAME + "\\" + LOG_FILE_NAME + "." + strDateFileLog + "." + LOG_FILE_EXTENSION;
            File file = new File(resultPath);
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
                if (!file.exists()) {
                    file.createNewFile();
                }
            }
            fstream = new FileWriter(resultPath, true);
            BufferedWriter fbw = new BufferedWriter(fstream);
            fbw.write(strDate + ": " + mes);
            fbw.newLine();
            fbw.close();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(SpecialControlLog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getDefaultLogPath() {
        BootstrapManager bootstrapManager = SpecialControlFactory.getBootstrapManager();
        return bootstrapManager.getLocalHome() + System.getProperty("file.separator") + ConfluenceHomeGlobalConstants.LOGS_DIR;
    }

    public static void outputStartTime(String actionName) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        outputLog("START ACTION: " + actionName);
        outputLog("DATE START: " + strDate);
        outputLog("TIME START: " + strSecond);
    }

    public static void outputEndTime(String actionName) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        outputLog("END ACTION: " + actionName);
        outputLog("DATE END: " + strDate);
        outputLog("TIME END: " + strSecond);
    }

    public static void outputTime(Boolean isStart, String... actionNames) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        if (actionNames.length > 0) {
            outputLog("=================================");
            if (isStart) {
                outputLog("START ACTION: " + actionNames[0]);
            } else {
                outputLog("END ACTION: " + actionNames[0]);
            }

            for (int i = 1; i < actionNames.length; i++) {
                outputLog("POSITION: " + actionNames[i]);
            }
            outputLog("DATE START: " + strDate);
            outputLog("TIME START: " + strSecond);
            outputLog("----------------------------------");
        }
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String aResultPath) {
        resultPath = aResultPath;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.confluence.plugins.util;

import com.atlassian.confluence.pages.Page;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author hailc
 */
public class XmlDocumentUtil {

    public static Document convertPageToXml(Page page, Boolean isRoot) throws Exception {
        return convertStringToXml(page.getBodyAsString(), isRoot);
    }

    public static Document convertStringToXmlWithoutUnEscapeHtmlEntities(String xmlString, Boolean isRoot) throws Exception {
        xmlString = "<kod>" + xmlString + "</kod>";
        // html document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        try {
            builder = factory.newDocumentBuilder();
            document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
        } catch (Exception ex) {
            try {
                org.jsoup.nodes.Document workdocument = org.jsoup.Jsoup.parse(xmlString);
                org.jsoup.helper.W3CDom w3cDom = new org.jsoup.helper.W3CDom();
                document = w3cDom.fromJsoup(workdocument);
            } catch (Exception e) {
                throw new RuntimeException(ex);
            }
        }
        // パーサー内のDOMツリーのドキュメントノードをdocumentにセット
        return document;
    }
    
    public static Document convertStringToXml(String xmlString, Boolean isRoot) throws Exception {
        if (xmlString != null) {
            StringBuffer buffer = new StringBuffer(xmlString);
            StringHtmlEscapeUtil.unEscapeHtmlEntities(buffer);
            xmlString = buffer.toString();
            xmlString = StringHtmlEscapeUtil.unEscapeHtmlAttribuites(xmlString);
            if (isRoot) {
                xmlString = "<kod>" + xmlString + "</kod>";
            }
        } else if (isRoot) {
            xmlString = "<kod></kod>";
        }
        // html document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        try {
            builder = factory.newDocumentBuilder();
            document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
        } catch (Exception ex) {
            try {
                org.jsoup.nodes.Document workdocument = org.jsoup.Jsoup.parse(xmlString);
                org.jsoup.helper.W3CDom w3cDom = new org.jsoup.helper.W3CDom();
                document = w3cDom.fromJsoup(workdocument);
            } catch (Exception e) {
                throw new RuntimeException(ex);
            }
        }
        // パーサー内のDOMツリーのドキュメントノードをdocumentにセット
        return document;
    }

    public static String convertXmlToString(Node document, Boolean isRemoveRoot, Boolean isXmlDeclaration) throws Exception {
        StringWriter sw = new StringWriter();
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            if (isXmlDeclaration == true) {
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            } else {
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            }
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(document), new StreamResult(sw));
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }

        sw.flush();
        if (!isRemoveRoot) {
            return sw.toString();
        }

        return sw.toString().replace("<kod>", "").replace("</kod>", "");
    }

    public static List<Element> getElementByTagName(Element e, String tagName) throws Exception {
        NodeList nodeList = e.getElementsByTagName(tagName);
        List<Element> lstResult = new ArrayList<Element>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node dataNode = nodeList.item(i);
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagName(Element e, String tagName) throws Exception {
        List<Element> lstResult = getElementByTagName(e, tagName);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }

    public static List<Element> getElemetByAttrName(Element e, String attrName) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getChildNodes();
        for (int y = 0; y < childNodes.getLength(); y++) {
            Node data = childNodes.item(y);
            Node attrFileName = data.getAttributes().getNamedItem(attrName);
            if (attrFileName == null || data.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            lstResult.add((Element) data);
        }
        return lstResult;
    }

    public static List<Element> getElementsByTagNameAndAttr(Element e, String tagName, String attrName) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getElementsByTagName(tagName);
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node dataNode = childNodes.item(i);
            if (attrName != null) {
                Node attrNode = dataNode.getAttributes().getNamedItem(attrName);
                if (attrNode == null) {
                    continue;
                }
            }
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagNameAndAttr(Element e, String tagName, String attrName) throws Exception {
        List<Element> lstResult = getElementsByTagNameAndAttr(e, tagName, attrName);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }

    public static List<Element> getElementsByTagNameAndAttr(Element e, String tagName, String attrName, String attrValue) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getElementsByTagName(tagName);
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node dataNode = childNodes.item(i);
            if (attrName != null && attrValue != null) {
                Node attrNode = dataNode.getAttributes().getNamedItem(attrName);
                if (attrNode == null) {
                    continue;
                }
                String attrNodeValue = attrNode.getNodeValue();
                if (attrNodeValue == null || attrNodeValue.isEmpty() || !attrNodeValue.equals(attrValue)) {
                    continue;
                }
            }
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagNameAndAttr(Element e, String tagName, String attrName, String attrValue) throws Exception {
        List<Element> lstResult = getElementsByTagNameAndAttr(e, tagName, attrName, attrValue);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }
}

package jp.co.kodnet.confluence.plugins.action;

import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import java.util.HashMap;
import java.util.Map;
import com.atlassian.confluence.pages.Page;
import jp.co.kodnet.confluence.plugins.connector.TranslationConnector;
import jp.co.kodnet.confluence.plugins.factory.SpecialControlFactory;
import jp.co.kodnet.confluence.plugins.util.CommonFunction;
import jp.co.kodnet.confluence.plugins.util.SpecialControlLog;

public class SpecialControlAction extends ConfluenceActionSupport implements Beanable {
    
    private String message;
    private boolean result;
    private String html;
    private Renderer renderer;
    
    @Override
    public Object getBean() {
        Map<String, Object> bean = new HashMap<String, Object>();
        bean.put("result", this.result);
        bean.put("html", this.html);
        bean.put("message", message);
        return bean;
    }

    public String renderTablePageUIControl() {
        try {
            ActionContext context = ActionContext.getContext();
            String spaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "spaceKey");
            Object reflection = TranslationConnector.execute(TranslationConnector.METHOD_GET_BASE_SPACE_KEY_BY_SPACE_KEY, spaceKey);
            String baseSpaceKey = spaceKey;
            if (reflection != null) {
                baseSpaceKey = (String) reflection;
            }
            
            Page specialControlPage = CommonFunction.getSpecialControlPage(baseSpaceKey);
            if (specialControlPage == null) {
                message = getText("kod.plugin.create.translationreplacemanagement.no.data");
                this.result = false;
                return SUCCESS;
            }

            this.html = this.renderer.render(specialControlPage);;
            this.result = true;
        } catch (Exception e) {
            SpecialControlLog.outputLogException(SpecialControlAction.class, e);
            message = e.getMessage();
            return SUCCESS;
        }
        return SUCCESS;
    }    
    
    public String checkPageSpecialControl() {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            Page curentPage = SpecialControlFactory.getPageManager().getPage(pageId);
            this.result = CommonFunction.isPageSpecialControl(curentPage);
            return SUCCESS;
        } catch (Exception e) {
            SpecialControlLog.outputLogException(SpecialControlAction.class, e);
            return SUCCESS;
        }
    }
    
    
    public Renderer getRenderer() {
        return renderer;
    }

    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }
}

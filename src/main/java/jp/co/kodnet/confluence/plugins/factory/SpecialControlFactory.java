package jp.co.kodnet.confluence.plugins.factory;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.SpaceLabelManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class SpecialControlFactory {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private static SpaceManager spaceManager;
    private static PageManager pageManager;
    private static SpaceLabelManager spaceLabelManager;
    private static LabelManager labelManager;
    private static BootstrapManager bootstrapManager;
    private static SettingsManager settingsManager;
    private static PluginAccessor pluginAccessor;
    private static TransactionTemplate transactionTemplate;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="constructor">
    public SpecialControlFactory(SpaceManager spaceManager,
            PageManager pageManager,
            SpaceLabelManager spaceLabelManager,
            LabelManager labelManager,
            BootstrapManager bootstrapManager,
            SettingsManager settingsManager,
            PluginAccessor pluginAccessor,
            TransactionTemplate transactionTemplate
    ) {
        SpecialControlFactory.spaceManager = spaceManager;
        SpecialControlFactory.pageManager = pageManager;
        SpecialControlFactory.spaceLabelManager = spaceLabelManager;
        SpecialControlFactory.labelManager = labelManager;
        SpecialControlFactory.bootstrapManager = bootstrapManager;
        SpecialControlFactory.settingsManager = settingsManager;
        SpecialControlFactory.pluginAccessor = pluginAccessor;
        SpecialControlFactory.transactionTemplate = transactionTemplate;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="set/get">
    public static SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public static void setSpaceManager(SpaceManager spaceManager) {
        SpecialControlFactory.spaceManager = spaceManager;
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    public static void setPageManager(PageManager pageManager) {
        SpecialControlFactory.pageManager = pageManager;
    }

    public static SpaceLabelManager getSpaceLabelManager() {
        return spaceLabelManager;
    }

    public static void setSpaceLabelManager(SpaceLabelManager spaceLabelManager) {
        SpecialControlFactory.spaceLabelManager = spaceLabelManager;
    }

    public static LabelManager getLabelManager() {
        return labelManager;
    }

    public static void setLabelManager(LabelManager labelManager) {
        SpecialControlFactory.labelManager = labelManager;
    }

    public static BootstrapManager getBootstrapManager() {
        return bootstrapManager;
    }

    public static void setBootstrapManager(BootstrapManager bootstrapManager) {
        SpecialControlFactory.bootstrapManager = bootstrapManager;
    }

    public static SettingsManager getSettingsManager() {
        return settingsManager;
    }

    public static void setSettingsManager(SettingsManager settingsManager) {
        SpecialControlFactory.settingsManager = settingsManager;
    }

    public static PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    public static void setPluginAccessor(PluginAccessor aPluginAccessor) {
        pluginAccessor = aPluginAccessor;
    }
    
    public static TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    public static void setTransactionTemplate(TransactionTemplate aTransactionTemplate) {
        transactionTemplate = aTransactionTemplate;
    } 
    // </editor-fold>

}

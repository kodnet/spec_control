/* global AJS */
AJS.$(document).ready(function () {
    var pageId = AJS.Meta.get('page-id');
    $(window).on('load', function () {
        if (typeof pageId !== 'undefined' && pageId !== null && pageId !== "") {
            checkDuplicateKey(pageId);
        }
    });
});

function checkDuplicateKey(pageId) {
    try {
        var isPageSpecialControl = checkPageSpecialControl(pageId);
        if (isPageSpecialControl === false) {
            return;
        }

        var content = $(document).find('#content');
        if (content.length === 0) {
            return;
        }

        var tables = $(content).find('table');
        if (tables.length === 0) {
            return;
        }
        var tableSpecialControl = $(tables)[0];
        var count = 0;
        var tr = $(tableSpecialControl).find('tr:has(td)');
        $(tr).removeClass('has-same-special-key');
        $(tr).each(function () {
            if ($(this).hasClass('has-same-special-key')) {
                return true;
            }
            var tdKeySeft = $(this).children('td:first');
            var tdKey = $(tdKeySeft).clone(true);

            var halfSpace = $(tdKey).find('span.half-width-space');
            if (halfSpace.length > 0) {
                $(halfSpace).text(' ');
            }

            var lineBreak = $(tdKey).find('span.line-break');
            if (lineBreak.length > 0) {
                $(lineBreak).remove();
            }

            var specialKey = $(tdKey).text();
            if (specialKey === "") {
                return true;
            }
            var validTd = $(tableSpecialControl).find('td:first-child').filter(function () {
                var tdClone = $(this).clone(true);
                var halfSpaceClone = $(tdClone).find('span.half-width-space');
                if (halfSpaceClone.length > 0) {
                    $(halfSpaceClone).text(' ');
                }

                var lineBreakClone = $(tdClone).find('span.line-break');
                if (lineBreakClone.length > 0) {
                    $(lineBreakClone).remove();
                }
                return $(tdClone).text() === specialKey;
            });
            if (validTd.length > 1) {
                $(validTd).closest('tr').addClass('has-same-special-key');
                count++;
            }
        });

        if (count > 0) {
            var message = AJS.I18n.getText('kod.plugin.create.specialcontrol.error.same.special.key');
            var contentMessage = $(document).find('#content #action-messages');
            if (contentMessage.length == 0) {
                $(document).find('#content').prepend('<div id="action-messages"></div>');
                contentMessage = $(document).find('#content #action-messages');
            }
            $(contentMessage).css('padding-bottom', '10px');
            AJS.messages.error(contentMessage, {
                title: message,
                body: '',
                closeable: false
            });
        }
    } catch (e) {
        console.log(e);
    }
}

function checkPageSpecialControl(pageId) {
    var isPageSpecialControl = false;
    try {
        AJS.$.ajax({
            type: 'GET',
            url: AJS.Meta.get("context-path") + "/specialcontrol/checkpagespecialcontrol.action",
            data: {pageId: pageId},
            async: false,
            success: function (data) {
                isPageSpecialControl = data.result;
            },
            error: function (e) {
                console.log(e);
            }
        });
    } catch (e) {
        console.log(e);
    }
    return isPageSpecialControl;
}
/* global tinymce, AJS, Confluence, tinyMCE */

//bind on initialization of editor
AJS.bind("init.rte", function () {
    InitMacroDialog();
    var macroBody = AJS.$('#body-special-control-macro');
    if (macroBody) {
        setTimeout(function () {
            initialize();
        }, 500);
    }
    function initialize() {
        InitMacroEvent();
    }
});

function InitMacroDialog() {
    var macroName = 'specialcontrolmacro';

    // 1. create dialog to add macro
    var dialog = new AJS.Dialog({
        width: 1000,
        height: 600,
        id: "special-control-macro"
    });

    // adds header for first page
    dialog.addHeader(AJS.I18n.getText("kod.plugin.specialcontrolmacro.preview.title"));

    // render macro boy
    var macroBody = Confluence.Templates.SpecialControlReview.SpecialControlReviewMacroTemplete({});

    dialog.addPanel("SinglePanel", macroBody, "specialcontrolPanel");

    AJS.$("#macro-preview-iframe").find('iframe').remove();

    // 2. add macro to editor
    var create = AJS.I18n.getText("kod.plugin.specialcontrolmacro.button.insert");
    dialog.addSubmit(create, function () {
        var selectData = AJS.$("#body-special-control-macro table tr.selected");
        if (typeof selectData === 'undefined' || selectData.length === 0) {
            return false;
        }
        var key = "";
        var value = "";
        var arr = [];
        $(selectData).find("td").each(function (index, item) {
            arr.push($(this).text());
        });

        key = arr[0];
        value = arr[1];

        tinymce.confluence.MacroUtils.insertMacro({
            contentId: AJS.Meta.get('content-id'),
            macro: {
                body: '',
                defaultParameterValue: value,
                name: macroName,
                params: {'specialTagKey': key, "specialTagValue": value}
            }
        });
        dialog.hide();
    });

    // hide dialog
    var cancel = AJS.I18n.getText("kod.plugin.specialcontrolmacro.button.cancel");
    dialog.addCancel(cancel, function () {
        $(".selected-page").val('');
        dialog.hide();
    });

    // 5. bind event to edit macro browser
    AJS.MacroBrowser.setMacroJsOverride(macroName, {opener: function (macro) {
            var valueSpecialKey = "";
            if (macro.params) {
                valueSpecialKey = macro.params.specialTagKey;
            }

            // open custom dialog
            dialog.show();
            var spaceKey = $('meta[name="confluence-space-key"]').attr("content");
            RenderMacroBody(spaceKey);
            
            AJS.$('.specialcontrolPanel').css('padding', '0px');
            AJS.$('.kod-title').val('');
            AJS.$("#macro-preview-iframe").find('iframe').remove();
            AJS.$("#body-special-control-macro table tr.selected").removeClass("selected");
            if (valueSpecialKey !== "") {
                var tdLangs = AJS.$("#body-special-control-macro table tr").find("td:first-child");
                if (tdLangs.length > 0) {
                    var tableLangs = $(tdLangs).filter(function () {
                        return $(this).text() === valueSpecialKey;
                    });
                    if (typeof tableLangs !== 'undefined') {
                        $(tableLangs).closest("tr").addClass("selected");
                    }
                }
            }
        }
    });
}

function RenderMacroBody(spaceKey) {
    try {
        AJS.$.ajax({
            type: "GET",
            url: AJS.Meta.get("context-path") + "/specialcontrol/getspeciallist.action",
            data: {
                spaceKey: spaceKey
            },
            success: function (data) {
                if (data.result === true) {
                    AJS.$("#body-special-control-macro").html(data.html);
                    var tableSpecialMacro = $("#body-special-control-macro table");
                    var count = 0;
                    if (tableSpecialMacro.length > 0) {
                        var tr = $(tableSpecialMacro).find('tr:has(td)');
                        $(tr).removeClass('has-same-special-key');
                        $(tr).each(function () {
                            if ($(this).hasClass('has-same-special-key')) {
                                return true;
                            }
                            var tdKey = $(this).children('td:first');
                            var specialKey = $(tdKey).text();
                            if (specialKey == "") {
                                return true;
                            }
                            var validTd = $(tableSpecialMacro).find('td:first-child').filter(function () {
                                return $(this).text() === specialKey;
                            });
                            if (validTd.length > 1) {
                                $(validTd).closest('tr').addClass('has-same-special-key');
                                count++;
                            }
                        });

                        if (count > 0) {
                            AJS.$("#special-control-message").append(AJS.I18n.getText('kod.plugin.create.specialcontrol.error.same.special.key'));
                            var closeIcon = $(".specialcontrol-message-no-data").find("span.icon-close");
                            if (closeIcon.length > 0) {
                                $(closeIcon).remove();
                            }
                            AJS.$(".specialcontrol-message-no-data").show();
                        }
                    }
                    AJS.$("#body-special-control-macro").css("padding", "15px");
                } else {
                    AJS.$("#ui-control-message").append(data.message);
                    AJS.$(".specialcontrol-message-no-data").show();
                }
            },
            error: function (data) {
                AJS.$("#ui-control-message").append(data.message);
                AJS.$(".specialTag-message-no-data").show();
            }
        });
    } catch (e) {
        console.log(e);
    }
}

function InitMacroEvent() {
    $(document).on("click", "#body-special-control-macro table tr:has(td)", function () {
        AJS.$("#body-special-control-macro table tr").each(function () {
            if ($(this).hasClass("selected")) {
                $(this).removeClass("selected");
            }
        });
        $(this).addClass("selected");
    });
}

